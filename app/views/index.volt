<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        {{ stylesheet_link('css/style.css') }}
        {{ javascript_include('js/jquery-ajax.min.js') }}
        <title>Merge</title>
    </head>
    <body>
        {{ content() }}
    </body>
</html>
