<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php echo $this->tag->stylesheetLink('css/style.css'); ?>
        <?php echo $this->tag->javascriptInclude('js/jquery-ajax.min.js'); ?>
        <title>Merge</title>
    </head>
    <body>
        <?php echo $this->getContent(); ?>
    </body>
</html>
