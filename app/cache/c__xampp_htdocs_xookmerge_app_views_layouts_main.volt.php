<div style="background: #ECF1F7;">
    <div id="contenedor" class="container">
        <img src="/../img/header_logo.png" style="padding-top: 30px; width: auto; float: left;">
        <h1 align="center" style="padding-right: 140px; padding-top: 30px; color: darkgray;">Merge de cuentas</h1>
    </div>
</div>
<?php echo $this->tag->image(array('img/LineaDivision_header.png', 'class' => 'img-responsive lineaimg', 'alt' => '-------')); ?>

<div class="container" style=" border-right: solid; border-left: solid; border-width: 1px; border-color: darkgray; padding: 0px;">
    <?php echo $this->getContent(); ?>
    <hr>
    <footer>
        <?php echo $this->tag->image(array('img/pie_pagina.png', 'class' => 'img-responsive img-pie')); ?>
        <p style="margin-left: 15px; margin-top: 15px;">&copy; Orbile 2015</p>
    </footer>
</div>