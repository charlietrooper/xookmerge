<?php echo $this->getContent(); ?>

 <h1 align="center">Merge de cuentas</h1>
<p>&nbsp;</p>
<?php echo $this->tag->form(array('index/merge')); ?>
<fieldset>
    <div width="100%" style="display: block; overflow: auto;">
        <table align="center" class="detalle table-bordered table-striped">
            <tr>
                <th><label for="items_per_page">Ingresa cuenta:</label></th>
                <td><?php echo $this->tag->textField(array('cuenta', 'id' => 'cuenta')); ?></td>
            </tr>
            <tr>
                <th><label for="items_per_page">Password:</label></th>
                <td><?php echo $this->tag->textField(array('contra', 'id' => 'contra')); ?></td>
            </tr>
        </table>
        <table align="center">
            <tr>
                <td colspan="3" style="text-align: right">
                    <ul class="pager">
                        <li class="pull-right">
                            <?php echo $this->tag->submitButton(array('Enviar', 'class' => 'btn btn-success', 'id' => 'ok')); ?>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
</fieldset>
</form>


<script type="text/javascript">

    $(document).ready(function(){
        $('#ok').on('click', function(){
            $cuenta = $('#cuenta').val();
            $contra = $('#contra').val();

            if($cuenta.trim() == '' || $contra.trim() == '')
            {
                event.preventDefault();
                alert("Requiere ingresar valores en ambos campos: \n 'ISBN' ó el 'ID del libro'");
            }
        });
    });
</script>