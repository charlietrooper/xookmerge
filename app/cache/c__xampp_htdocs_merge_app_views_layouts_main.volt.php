<nav class="navbar navbar-default navbar-inverse" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/../merge">Inicio</a>
        </div>
    </div>
</nav>

<div class="container">
    <?php echo $this->getContent(); ?>
    <hr>
    <footer>
        <p>&copy; Orbile 2015</p>
    </footer>
</div>
