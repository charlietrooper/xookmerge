<?php echo $this->getContent(); ?>
<div class="row">
    <div class="col-lg-5 col-md-6 col-sm-12">
        <div align="center" style="padding: 15px;">
            <p style="width: 450px; text-align: justify; color: gray; padding-top: 24px; font-size: small; line-height: 21px;">
                Ingrese sus cuentas de Gandhi y de Porrúa  con sus respectivas contraseñas para que se lleve a cabo el la unión de sus cuentas,
                y la que elija como primaria, será la que utilizará a partir de ahora.
            </p>
        </div>

        <?php echo $this->tag->form(array('index/merge')); ?>
            <div width="100%" style="display: block; overflow: auto;">
                <table align="center" class="imagen" style=" border: hidden; width: 420px;">
                    <tr style="background-color: transparent">
                        <th style="text-align: center; padding-top: 30px;"><label for="cuenta1">Ingrese la cuenta que<br/> elegirá como primaria:</label></th>
                        <th style="text-align: center; padding-top: 30px;"><label for="cuenta2">Ingrese la cuenta que<br/> elegirá como secundaria:</label></th>
                    </tr>
                    <tr>
                        <td style="text-align: center; padding-bottom: 30px;"><?php echo $this->tag->textField(array('cuenta1', 'id' => 'cuenta1')); ?></td>
                        <td style="text-align: center; padding-bottom: 30px;"><?php echo $this->tag->textField(array('cuenta2', 'id' => 'cuenta2')); ?></td>

                    </tr>
                    <tr style="background-color: transparent">
                        <th style="text-align: center; "><label for="contra1">Password primario:</label></th>
                        <th style="text-align: center; "><label for="contra2">Password secundario:</label></th>
                    </tr>
                    <tr>
                        <td style="text-align: center; color: darkgray; padding-bottom: 30px;"><?php echo $this->tag->passwordField(array('contra1', 'id' => 'contra1')); ?></td>
                        <td style="text-align: center; color: darkgray; padding-bottom: 30px;"><?php echo $this->tag->passwordField(array('contra2', 'id' => 'contra2')); ?></td>
                    </tr>
                </table>
                <table align="center">
                    <tr>
                        <td colspan="3" style="text-align: right">
                            <ul class="pager">
                                <li class="pull-right">
                                    <?php echo $this->tag->submitButton(array('merge', 'class' => 'btn btn-primary', 'id' => 'ok')); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
    <div class="col-lg-7 col-md-6 col-sm-12" style="text-align: center;">
        <?php echo $this->tag->image(array('img/logo_inicio_merge.png', 'class' => 'img-responsive')); ?>
    </div>
</div>



<script type="text/javascript">

    $(document).ready(function(){
        $('#ok').on('click', function(){
            $cuenta1 = $('#cuenta1').val();
            $cuenta2 = $('#cuenta2').val();
            $contra1 = $('#contra1').val();
            $contra2 = $('#contra2').val();

            if($cuenta1.trim() == '' || $contra1.trim() == '' || $contra1.trim() == '' || $contra2.trim() == '')
            {
                event.preventDefault();
                alert("Requiere ingresar valores en todos los campos");
            }
        });
    });
</script>